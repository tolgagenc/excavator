using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    float minRoadX = -1.1f;
    float maxRoadX = 1.1f;

    float screenDis;
    float roadDisX;
    float disDifX;

    float minDisX;
    float maxDisX;
    float? firstMousePosX;

    [SerializeField]
    GameObject upperBody;
    [SerializeField]
    GameObject barrel;
    [SerializeField]
    GameObject rocket;

    [SerializeField]
    ParticleSystem gunFire;

    RaycastHit hit;

    // Start is called before the first frame update
    void Start()
    {
        screenDis = (Screen.width * 7.5f / 10f) - (Screen.width * 2.5f / 10f);

        roadDisX = Mathf.Abs(maxRoadX - minRoadX);

        disDifX = screenDis / roadDisX;
    }

    // Update is called once per frame
    void Update()
    {
        MouseControlX();
    }

    private void FixedUpdate()
    {
        if(Physics.Raycast(new Vector3(upperBody.transform.position.x, upperBody.transform.position.y, upperBody.transform.position.z + 0.5f), upperBody.transform.forward, out hit, 10f))
        {
            if ( hit.transform.tag == "Obstacle")
            {
                hit.transform.tag = "Destroyed Obstacle";

                GameObject newRocket = Instantiate(rocket, barrel.transform.position, Quaternion.Euler(upperBody.transform.rotation.x, 90, 90));

                gunFire.Play();

                newRocket.GetComponent<Rigidbody>().velocity = transform.TransformDirection(upperBody.transform.forward * 20);
            }
        }

        Debug.DrawRay(new Vector3(upperBody.transform.position.x, upperBody.transform.position.y, upperBody.transform.position.z + 0.5f), upperBody.transform.forward, Color.black, 1f);
    }

    void MouseControlX()
    {
        if (Input.GetMouseButtonDown(0))
        {
            firstMousePosX = Input.mousePosition.x;
        }
        if (Input.GetMouseButton(0))
        {
            if (firstMousePosX > Input.mousePosition.x)    // Sol
            {
                minDisX = transform.position.x - (((float)firstMousePosX - Input.mousePosition.x) / disDifX);

                if (minDisX >= minRoadX)
                {
                    upperBody.transform.rotation = Quaternion.Euler(upperBody.transform.rotation.x, -90 * minDisX / minRoadX, upperBody.transform.rotation.z);
                    transform.position = new Vector3(minDisX, transform.position.y, transform.position.z);
                    firstMousePosX = Input.mousePosition.x;
                }
                else
                {
                    upperBody.transform.rotation = Quaternion.Euler(upperBody.transform.rotation.x, - 90 * minRoadX / minRoadX, upperBody.transform.rotation.z);
                    transform.position = new Vector3(minRoadX, transform.position.y, transform.position.z);
                }
            }
            else if (firstMousePosX < Input.mousePosition.x)   // Sa�
            {
                maxDisX = transform.position.x + ((Input.mousePosition.x - (float)firstMousePosX) / disDifX);

                if (maxDisX <= maxRoadX)
                {
                    upperBody.transform.rotation = Quaternion.Euler(upperBody.transform.rotation.x, 90 * maxDisX / maxRoadX, upperBody.transform.rotation.z);
                    transform.position = new Vector3(maxDisX, transform.position.y, transform.position.z);
                    firstMousePosX = Input.mousePosition.x;
                }
                else
                {
                    upperBody.transform.rotation = Quaternion.Euler(upperBody.transform.rotation.x, 90 * maxRoadX / maxRoadX, upperBody.transform.rotation.z);
                    transform.position = new Vector3(maxRoadX, transform.position.y, transform.position.z);
                }
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            firstMousePosX = null;
        }
    }
}
