using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Destroyed Obstacle" || other.tag == "Obstacle")
        {
            other.gameObject.SetActive(false);
            transform.gameObject.SetActive(false);
        }
    }
}
