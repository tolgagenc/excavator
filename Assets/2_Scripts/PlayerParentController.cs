using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParentController : MonoBehaviour
{
    [SerializeField]
    float playerSpeed = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    void FixedUpdate()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * playerSpeed);
    }
}
